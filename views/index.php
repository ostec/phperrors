<!DOCTYPE html>
<html lang="de">
<head>
    <title>PHP_ERRORS</title>
    <meta charset="utf-8">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="<?=$this->getData('util')->getHostWebRoot()?>css/sb-admin-2.css" type="text/css"/>
    <link rel="stylesheet" href="<?=$this->getData('util')->getHostWebRoot()?>css/bootstrap.min.css" type="text/css"/>
    <link rel="stylesheet" href="<?=$this->getData('util')->getHostWebRoot()?>css/style.css" type="text/css"/>
    <link rel="icon" href="<?=$this->getData('util')->getHostWebRoot()?>css/images/favicon.png" type="image/png">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="<?=$this->getData('util')->getHostWebRoot()?>js/bootstrap.min.js"></script>
    <script src="<?=$this->getData('util')->getHostWebRoot()?>js/htmlEntitiesConfigForSearch.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-sanitize.js"></script>
    <script src="<?=$this->getData('util')->getHostWebRoot()?>js/headroom.js"></script>
</head>
<body>
    <div id="content">
        <div ng-app="myApp" ng-controller="dataCtrl">
            <div id="dialogFilter" class="hide"></div>
            <div id="dialog" class="hide panel" data-key="{{dialogKey}}">
                <div id="dialogHeader">
                    <div>
                        <button class="btn btn-success" id="dialogClose" ng-click='closeDetail()'>Schlie&szlig;en</button>
                        <button class="btn btn-danger" id="dialogDelete" ng-click='deleteDialog()'>L&ouml;schen</button>
                    </div>
                    <div>
                        <button class="btn btn-warning" id="dialogDelete" ng-click='rawDialog()'>Rohdaten</button>
                    </div>
                </div>
                <div id="dialogContent">
                </div>
            </div>
            <div id="headerGroup" class="{{showHamburger}}">
                <?php
                if(array_key_exists('server',$this->getData('config')->asArray()) && (is_array($this->getData('config')->server->asArray()) && 0 < count($this->getData('config')->server->asArray()))) : ?>
                    <div id="tabs" class="hamburger">
                        <ul class="nav nav-tabs">
                            <?php foreach ($this->getData('config')->server->asArray() as $serverName => $serverUrl) : ?>
                                <li class="<?php echo (strstr($serverUrl, $this->getData('server')) !== false ? 'active' : '')?>">
                                    <a href="<?php echo htmlentities($serverUrl)?>"><?php echo htmlentities($serverName)?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
                <header class="well visibleHeader">
                    <div class="buttonGroup">
                        <button class="btn-danger btn" ng-click='deleteRow()'>L&ouml;schen</button>
                        <button class="btn-default btn hamburger" ng-click='startAndRefresh()'>Aktualisieren</button>
                        <button id="hamburger" ng-click='toggleHamburger()' class="btn btn-warning glyphicon glyphicon-menu-hamburger"></button>
                    </div>
                    <div id="autoComplete" class="hamburger">
                        <span  ng-click="saveSearchString()" class="saveString glyphicon glyphicon-floppy-disk"></span>
                        <input data-toggle="tooltip"  data-placement="bottom" title="{{searchText}}" id="volltextSuche" class="form-control" ng-model="searchText" type="text" ng-keyup="searchErrors(searchText)" placeholder="Suche"/>
                        <button id="showSearchStrings" class="form-control" ng-click="toggleSearchList()"><span class="glyphicon glyphicon-chevron-down"></span></button>
                        <div id="searchList" class="form-control {{searchListOpen}}" >
                            <div  ng-repeat="option in options" data-key="{{option}}"><span  ng-click="searchSelect(option)" class="text">{{option}}</span><span ng-click="deleteSearchString(option)" class="deleteString glyphicon glyphicon-floppy-remove"></span></div>
                        </div>
                    </div>
                    <div id="logo"><img src="<?=$this->getData('util')->getHostWebRoot()?>css/images/ostecLogo.png" alt="ostec.de"></div>
                    <div id="errorCount" class="hamburger"><p><span class="text-success" title="Fehler gesamt" data-toggle="tooltip" data-placement="bottom">{{errorCount}}&nbsp;|</span>&nbsp;<span class="text-success" title="Gefilterte Fehler" data-toggle="tooltip" data-placement="bottom">{{errorFilterCount}}&nbsp;|</span>&nbsp;<span class="text-danger" title="Kritische Fehler" data-toggle="tooltip" data-placement="bottom">{{criticalErrorCount}}</span></p></div>
                </header>
            </div>
            <div id="errorList">
                <div ng-repeat="x in data" class="errorElement well {{x.selected}} {{x.fatal}}" data-key="{{x.key}}" refresh-directive>
                    <div>
                        <div class="checkbox" ng-click="toggleSelect(x)"><input type="checkbox" class="selector {{x.checked}}"></div>
                        <div class="dateTime org" ng-bind-html="x.time" ng-click='showDetail(x)'></div>
                        <div class="dateTime short" ng-bind-html="x.timeShort"></div>
                        <div class="dateTime veryShort" ng-bind-html="x.timeVeryShort"></div>
                        <div class="metaData" ng-click='showDetail(x)'>
                            <span ng-bind-html="sanitize(x.title)"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            var factory = {
                'base' : window.location.href
            };
            $("body").attr("data-servername","<?php echo htmlentities($this->getData('server'))?>");
        </script>
    </div>
    <script src="<?= $this->getData('util')->getHostWebRoot()?>js/phpErrors.js"></script>
</body>
