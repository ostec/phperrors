<?php
$path = realpath(__DIR__ . '/../') . '/';

if(is_dir(!$path . 'build')){
    mkdir(!$path . 'build');
}

if(file_exists($path . 'build/phperrors.phar')){
    unlink($path . 'build/phperrors.phar');
}

$files = array('index.php', 'style.css');
$directorys = array('config', 'js', 'src', 'views', 'vendor');

$phar = new Phar($path . 'build/phperrors.phar');
foreach($files as $file){
    $phar->addFile($path . $file, $file);
}

$phar->buildFromDirectory($path, '#^' . preg_quote($path) . '(' . implode('|', $directorys) . ')#');
$phar->setStub(file_get_contents(__DIR__ . '/stub.php'));
