<?php
$pharName = 'phperrors.phar';
Phar::interceptFileFuncs();
Phar::webPhar(
    $pharName, 
    'index.php', 
    null, 
    array(
        'php' => Phar::PHP, 
        'css' => 'text/css',
        'js'  => 'application/x-javascript',
    ), 
    function() use ($pharName){
        $request = $_SERVER['REQUEST_URI'];
        $request = preg_replace('#^\/' . preg_quote($pharName) . '\/#', '', $request);
        
        if (file_exists('phar://' . __DIR__ . '/' . $pharName . '/' . $request)){
            return $request;
        }
        else{
            return 'index.php';
        }
    }
);
__HALT_COMPILER();
