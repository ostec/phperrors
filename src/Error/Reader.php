<?php

class Error_Reader extends Error_Path
{
    public function getData()
    {
        $return = array();
        $tmp    = $this->read();

        foreach ($tmp as $entry) {
            if (preg_match('#\.head.err$#', $entry)) {
                $return[] = $this->getEntry($entry);
            } elseif (preg_match('#\.fatal.err$#', $entry)) {
                $return[] = $this->getFatalEntry($entry);
            }
        }
        return $return;
    }

    public function getFilteredData($data)
    {
        $return = array();

        foreach ($data as $entry) {
            if (strpos($entry, 'fatal') !== false) {
                $return[] = $this->getFatalEntry($entry.'.err');
            } else {
                $return[] = $this->getEntry($entry);
            }
        }
        return $return;
    }

    /**
     * @return int
     */
    public function getCountData()
    {
        if (is_dir($this->path)) {
            $command = 'find '.$this->path.' -type f -not -name "*body*" | wc -l';
            exec($command, $retvar);
            return intval($retvar[0]);
        }
        return 0;
    }

    /**
     * @param $filter
     * @return int
     */
    public function getCountCriticalData($filter)
    {
        if (is_dir($this->path)) {
            $command = 'ls -l '.$this->path.'*'.$filter.'.err | wc -l';
            exec($command, $retvar);
            return intval($retvar[0]);
        }
        return 0;
    }

    private function getFatalEntry($entry)
    {
        $obj        = new stdClass();
        $obj->key   = $this->getId($entry, '.fatal.err');
        $obj->title = 'Fatal Error';
        $obj->time  = $this->getTime($entry);
        $obj->key .= '.fatal';
        $obj->path = $this->getPath($obj->key.'.err');
        $obj->name = $obj->key.'.err';
        return $obj;
    }

    private function getEntry($entry)
    {
        $obj        = new stdClass();
        $obj->key   = $this->getId($entry);
        $obj->title = $this->getContent($obj->key.'.head.err');
        $obj->time  = $this->getTime($entry);
        $obj->path  = $this->getPath($obj->key.'.head.err');
        $obj->name  = $obj->key.'.head.err';
        return $obj;
    }

    private function getId($entry, $replace = '.head.err')
    {
        $tmp = explode(' ', $entry);
        $tmp = array_pop($tmp);
        return str_replace($replace, '', $tmp);
    }

    private function getContent($name)
    {
        $content = file_get_contents($this->path.$name);
        return htmlentities($content, ENT_COMPAT, 'utf-8');
    }

    private function getTime($str)
    {
        $tmp = explode(' ', $str);

        array_pop($tmp);
        $time = array_pop($tmp);
        $date = array_pop($tmp);

        return date('Y-m-d H:i:s', strtotime($date.' '.$time));
    }

    private function getPath($name)
    {
        return $this->path.$name;
    }

    public function read()
    {
        if (is_dir($this->path)) {
            $command = 'ls -lt --time-style=long-iso --hide=*body* '.$this->path;
            exec($command, $retvar);
            unset($retvar[0]);
            return $retvar;
        }
        return array();
    }
}
