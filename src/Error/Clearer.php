<?php

class Error_Clearer extends Error_Path
{
    public function clear($entry)
    {
        if (preg_match('#.fatal$#', $entry)) {
            $this->delete($entry.'.err');
        } else {
            $this->delete($entry.'.head.err');
            $this->delete($entry.'.body.err');
        }
    }

    public function clearAll()
    {
        $command = 'rm '.$this->path.'*.err';
        exec($command);
    }

    protected function delete($file)
    {
        if (!empty($file)) {
            if (file_exists($this->path.$file)) {
                if (is_readable($this->path.$file)) {
                    unlink($this->path.$file);
                    return true;
                } else {
                    throw new Exception('Not Readable');
                }
            } else {
                throw new Exception('Not Exists');
            }
        } else {
            throw new Exception('Not File');
        }
    }
}
