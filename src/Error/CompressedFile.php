<?php

class Error_CompressedFile extends Error_File
{
    public function getContent($filename)
    {
        $str = parent::getContent($filename);

        if (preg_match('#^ERROR#', substr($str, 0, 10))) {
            return $str;
        }

        if (preg_match('#\.gz$#', $filename)) {
            return gzdecode($str);
        }

        return 'ERROR';
    }
}
