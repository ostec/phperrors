<?php

/**
 * @author thomas
 * @since 15.04.2013
 * @package
 */
class Error_Util
{
    private $serverData = null;
    
    public function __construct($serverData)
    {
        $this->serverData = $serverData;
    }
    
    public function getWebRoot()
    {
        $uri = dirname($this->serverData['SCRIPT_NAME']);
        if(preg_match('#\.phar$#', $this->serverData['REQUEST_URI'])){
            $uri = $this->serverData['REQUEST_URI'];
        }
        
        $uri = preg_replace('#[/]*$#', '', $uri) . '/';
        return $uri;
    }
    
    public function getHostWebRoot()
    {
        $str = '//' . $this->serverData['HTTP_HOST'] . $this->getWebRoot();
        return $str;
    }
}
