<?php

class Error_File extends Error_Path
{
    public function getContent($filename)
    {
        $filename = $this->path.$this->getFilenameWithSuffix($filename);

        if (file_exists($filename)) {
            if (is_readable($filename)) {
                return file_get_contents($filename);
            } else {
                return 'Not Readable "'.$filename.'"';
            }
        }
        return 'File Not Found "'.$filename.'"';
    }

    protected function getFilenameWithSuffix($filename)
    {
        if (preg_match('#.fatal$#', $filename)) {
            return $filename.'.err';
        }
        return $filename.'.body.err';
    }
}

