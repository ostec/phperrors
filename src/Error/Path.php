<?php

abstract class Error_Path
{
	protected $path = null;
	
	/**
	 * @param string $path
	 */
	public function __construct($path)
	{
		$this->path = $path;
		if(!preg_match('#\/$#', $this->path)){
			$this->path .= '/';
		}
	}
}	