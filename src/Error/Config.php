<?php

class Error_Config
{
    private $data;
    
    public function __construct(array $data)
    {
        $this->data = $data;
    }
    
    public function __get($name)
    {
        if(isset($this->data[$name])){
            if(is_array($this->data[$name])){
                return new Error_Config($this->data[$name]);
            }
            return $this->data[$name];
        }
        return null;
    }

    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    public function asArray()
    {
        return $this->data;
    }
}
