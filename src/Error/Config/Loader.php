<?php

class Error_Config_Loader
{
    private $path = null;
    
    public function __construct(array $path)
    {
        $this->path = $path;
    }
    
    public function load()
    {
        foreach($this->path as $entry)
        {
            if (file_exists($entry)) {
                return new Error_Config(
                    json_decode(file_get_contents($entry), true)
                );
            }
        }
        new Error_Config(array());
    }
}
