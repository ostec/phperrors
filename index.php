<?php

use Slim\Slim;

include __DIR__.'/src/Error/Config/Loader.php';
include __DIR__.'/src/Error/Config.php';

date_default_timezone_set('Europe/Berlin');

$configLoader = new Error_Config_Loader(
    array(
        '/etc/phperrors.config.json',
        preg_replace(
            '#'.preg_quote($_SERVER['SCRIPT_NAME'], '#').'$#',
            '',
            $_SERVER['SCRIPT_FILENAME']
        ).'/phperrors.config.json',
        __DIR__.'/../../../../../phperrors.config.json',
        __DIR__.'/../../phperrors.config.json',
        __DIR__.'/config/config.dist.json',
        __DIR__.'/config/config.json'
    )
);
$config       = $configLoader->load();

if ($config->autoloadPath != '') {
    include __DIR__.$config->autoloadPath;
} else {
    if (file_exists(__DIR__.'/vendor/autoload.php')) {
        include __DIR__.'/vendor/autoload.php';
    } else {
        include __DIR__.'/../../autoload.php';
    }
}

$util = new Error_Util($_SERVER);

$app = new Slim(
    array(
        'templates.path' => __DIR__.'/views/'
    )
);

$app->get(
    '/',
    function () use (&$app, &$config, &$util) {
        $app->render(
            'index.php',
            array(
                'server' => $_SERVER['SERVER_NAME'],
                'config' => $config,
                'util'   => $util
            )
        );
    }
);

$app->get(
    '/api/list',
    function () use (&$config) {
        $reader = new Error_Reader($config->errorPath);
        header('Content-Type: application/json');
        echo json_encode($reader->getData());
    }
);
$app->get(
    '/api/count',
    function () use (&$config) {
        $reader = new Error_Reader($config->errorPath);
        header('Content-Type: application/json');
        echo json_encode($reader->getCountData());
    }
);
$app->get(
    '/api/count/critical',
    function () use (&$config) {
        $reader = new Error_Reader($config->errorPath);
        header('Content-Type: application/json');
        echo json_encode(
            $reader->getCountCriticalData(
                isset($config->countCriticalFilter) ? $config->countCriticalFilter : 'fatal'
            )
        );
    }
);
$app->get(
    '/api/error/:key',
    function ($key) use (&$config) {
        $file = new Error_File($config->errorPath);
        echo $file->getContent($key);
    }
);
$app->post(
    '/api/error/:key/delete',
    function ($key) use (&$config) {
        $clearer = new Error_Clearer($config->errorPath);
        $clearer->clear($key);
    }
);
$app->post(
    '/api/error/delete',
    function () use (&$config) {
        $clearer = new Error_Clearer($config->errorPath);

        $data = json_decode(file_get_contents('php://input'));
        if (is_array($data)) {
            foreach ($data as $key) {
                $clearer->clear($key);
            }
        } else {
            $clearer->clearAll();
        }
    }
);

$app->run();
