
var app = angular.module('myApp', ['ngSanitize'])
    .directive('refreshDirective',function(){
        return function(scope, element, attrs) {
            if (scope.$last){
                setTimeout(function(){
                    setHeaderMargin();
                    doZebra();
                    $(".errorElement .checked").prop("checked", true);
                    scope.searchErrors(scope.searchText);
                    if($("body").width()<800){
                        var timeout = null;
                        var downEvent;
                        var upEvent;
                        var errorItem = $("body").find(".dateTime, .metaData");
                        if('ontouchstart' in window || navigator.msMaxTouchPoints|| navigator.maxTouchPoints){
                            downEvent = 'touchstart pointerdown MSPointerDown';
                            upEvent = 'touchend pointerup MSPointerUp'
                        }
                        else {
                            downEvent = 'mousedown';
                            upEvent = 'mouseup'
                        }
                        errorItem.off(upEvent);
                        errorItem.off(downEvent);
                        var noDetail = false;
                        errorItem.on(downEvent,function(e){
                            var self = $(this);
                            if(!scope.mobileSelectmodus){
                                timeout = window.setTimeout(function(){
                                    self.parents(".errorElement").toggleClass("selected");
                                    noDetail = true;
                                    scope.mobileSelectmodus = true;
                                }, 750);
                            }
                            else {
                                var tmp = $(".selected").length;
                                self.parents(".errorElement").toggleClass("selected");
                                if($(".selected").length == 0 && tmp == 1){
                                    scope.mobileSelectmodus = false;
                                    noDetail = true;
                                }
                            }

                        });
                        errorItem.on(upEvent, function (e) {
                            window.clearTimeout(timeout);
                            if(!noDetail && !scope.mobileSelectmodus){
                                scope.showDetail(e,true);
                            }
                            else {
                                noDetail = false;
                            }
                        });
                    }

                },1);
            }
        };
    });
app.controller('dataCtrl',['$scope','$http','$sce',function($scope, $http, $sce) {

    $scope.allowSelect = true;
    $scope.originalTitles = {};
    $scope.shiftStart = false;
    $scope.errorCount = 0;
    $scope.errorFilterCount = 0;
    $scope.refreshTime = 200;
    $scope.selectList = [];
    $scope.searchListOpen = "";
    $scope.mobileSelectmodus = false;

    $(document).bind("keydown", function(event) {
        $scope.shiftPressed = event.shiftKey;
    });
    $(document).bind("keyup", function() {
        $scope.shiftPressed = false;
    });

    $scope.sanitize = function(html) {
        return $sce.trustAsHtml(html);
    };


    $scope.startAndRefresh = function(){
        $scope.selectList = [];
        $(".selected").each(function(){
            $scope.selectList.push($(this).attr("data-key"));
        });

        $http.get(factory.base + 'api/list').then(function(response) {
            for(var i=0;i<response.data.length;++i){
                if(response.data.hasOwnProperty(i)){
                    tmpdate = response.data[i].time.split(" ")[0].split("-");
                    tmptime = response.data[i].time.split(" ")[1].split(":");
                    response.data[i].timeShort = tmpdate[1]+"."+tmpdate[2]+".'"+tmpdate[0][1]+tmpdate[0][2]+"<br />"+tmptime[0]+":"+tmptime[1]+" Uhr";
                    response.data[i].timeVeryShort = tmpdate[1]+"."+tmpdate[2]+"."+"<br />"+tmptime[0]+":"+tmptime[1];
                    if($.inArray(response.data[i].key,$scope.selectList) != -1){
                        response.data[i].selected = "selected";
                        response.data[i].checked = "checked";
                    }
                    else {
                        response.data[i].selected = "";
                        response.data[i].checked = "";
                    }
                    if(response.data[i].key.indexOf("fatal") != -1){
                        response.data[i].fatal = "fatal";
                    }
                    else {
                        response.data[i].fatal = "";
                    }
                }
                //gw-digital Schmutz
                response.data[i].title = response.data[i].title.replace(new RegExp("&amp;", 'g'), "&");
            }
            $scope.data = response.data;

        });
        $http.get(factory.base + 'api/count').then(function(response) {
            $scope.errorCount = parseInt(response.data);
        });
        $http.get(factory.base + 'api/count/critical').then(function(response) {
            $scope.criticalErrorCount = response.data;
        });
        $(".errorElement .checked").prop("checked", true);
    };

    $scope.refreshSearchlist = function(){
        $scope.options = [];
        for(var i in localStorage)
        {
            if(localStorage.hasOwnProperty(i)){
                $scope.options.push(i);
            }
        }
    };

    $scope.toggleSearchList = function(){
        if($scope.searchListOpen==""){
            $scope.searchListOpen = "open";
        }
        else {
            $scope.searchListOpen = "";
            $("#searchList").removeClass("openSelect");
        }
    };

    $scope.searchSelect = function(text){
        $scope.searchText = text;
        $scope.toggleSearchList();
        $scope.searchErrors(text);
        $("#searchList").removeClass("openSelect");
        $scope.searchListOpen = "";
    };

    $scope.deleteSearchString = function(e){
        localStorage.removeItem(e);
        $scope.refreshSearchlist();
    };

    $scope.saveSearchString = function(e){
        if($scope.searchText.length >2){
            localStorage.setItem($scope.searchText,$scope.searchText);
            $scope.refreshSearchlist();
        }
    };

    $scope.showDetail = function(e,mobile){
        if($("body").width()>799 || mobile){
            var key;
            if(mobile){
                key = $(e.target).parents(".errorElement").attr("data-key");
            }
            else {
                key = e.key;
            }
            $scope.allowSelect=false;
            $http.get(factory.base + 'api/error/'+ key).then(function(response) {
                $scope.dialogKey= key;
                if(e.fatal=="fatal"){
                    $("#dialogContent").html(response.data);
                    $("#dialog, #dialogFilter").removeClass("hide");
                }
                else {
                    $("#dialogContent").html($scope.parseDetailInformation(response.data));
                    $("body").addClass("noScroll");
                    $("#dialog, #dialogFilter").removeClass("hide");
                    $("body").find(".panel-heading:not(.noPointer)").click(function () {
                        $(this).next(".panel-body").toggleClass("open");
                        $(this).toggleClass("glyphicon-plus");
                        $(this).toggleClass("glyphicon-minus");
                    });
                    $scope.allowSelect=true;
                }
            });
        }
    };

    $scope.rawDialog = function(){
        $http.get(factory.base + 'api/error/'+ $scope.dialogKey).then(function(response) {
            if (typeof response.data == "object") {
                $("#dialogContent").html(JSON.stringify(response.data, null, 4).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;'));
            } else {
                $("#dialogContent").html(response.data);
            }
        });
    };

    $scope.closeDetail = function(e){
        $("#dialog, #dialogFilter").addClass("hide");
        $("body").removeClass("noScroll");
        window.setTimeout(function(){
            $("#dialogContent").html("");
        },500);

    };

    $scope.toggleSelect = function(e, mobile){
        var element = $(".errorElement[data-key='"+ e.key+"']");
        if($("body").width()>799 || mobile){
            element.toggleClass("selected");
            if(!$scope.shiftPressed){
                $(".shiftStart, .shiftEnd").removeClass("shiftStart shiftEnd");
                element.addClass("shiftStart");
                $scope.shiftStart = true;
            }

            if($scope.shiftPressed){
                document.getSelection().removeAllRanges();
                if($scope.shiftStart){
                    element.addClass("shiftEnd");
                    $scope.selectArea();
                    $(".shiftStart").removeClass("shiftStart");
                    element.addClass("shiftStart");
                } else {
                    element.addClass("shiftStart");
                    $scope.shiftStart = true;
                    $(".shiftEnd").removeClass("shiftEnd");
                }
            }
        }
    };

    $scope.selectArea = function(){
        var shiftStart = $(".shiftStart").index(".errorElement");
        var shiftEnd = $(".shiftEnd").index(".errorElement");

        if(shiftEnd<shiftStart){
            tmp = shiftStart;
            shiftStart = shiftEnd;
            shiftEnd = tmp;
        }

        for(i = shiftStart;i<=shiftEnd;++i){
            $($(".errorElement")[i]).addClass("selected");
            $($(".errorElement")[i]).find("input").prop("checked",true);
        }

        $(".shiftStart, .shiftEnd").removeClass("shiftStart shiftEnd");
    };

    $scope.deleteRow = function() {
        var selectedErrors = $(".errorElement.selected");
        var filteredErrors = $(".errorElement.foundSearch");
        var selectedFromFilteredErrors = $(".errorElement.foundSearch.selected");
        if (selectedErrors.length == 0 && filteredErrors.length == 0 && confirm(unescape("Warnung%3A Alle Errors l%F6schen%3F"))) {
            $scope.finalDelete(null);
        }
        else if (selectedErrors.length != 0 && filteredErrors.length == 0 && confirm(unescape("Ausgew%E4hlte" + (selectedErrors.length == 1 ? "n" : " " + selectedErrors.length) + " Error" + (selectedErrors.length > 1 ? "s" : "") + " l%F6schen%3F"))) {
            $scope.finalDelete(selectedErrors);
        }
        else if (selectedFromFilteredErrors.length != 0 && confirm(unescape("Ausgew%E4hlte" + (selectedFromFilteredErrors.length == 1 ? "n" : " " + selectedFromFilteredErrors.length) + " Error" + (selectedFromFilteredErrors.length > 1 ? "s" : "") + " l%F6schen%3F"))) {
            $scope.finalDelete(selectedFromFilteredErrors);
        }
        else if(filteredErrors.length > 0 && confirm(unescape("Gefilterte" + (filteredErrors.length == 1 ? "r" : " " + filteredErrors.length) + " Error" + (filteredErrors.length > 1 ? "s" : "") + " l%F6schen%3F"))) {
            $scope.finalDelete(filteredErrors);
        }
    };

    $scope.deleteDialog = function(e){
        $http.post(factory.base + 'api/error/'+$scope.dialogKey+'/delete').then(function(response){
            $scope.startAndRefresh();
            $scope.closeDetail();
        });
    };

    $scope.finalDelete = function(e){
        if(e == null){
            $http.post(factory.base + 'api/error/delete').then(function(response){
                $scope.startAndRefresh();
            });
        } else {
            var fileKeys = [];
            for( i = 0; i < e.length; ++i){
                $(".errorElement[data-key='"+$(e[i]).attr("data-key")+"']").remove();
                fileKeys.push($(e[i]).attr("data-key"));
            }
            $http.post(factory.base + 'api/error/delete', JSON.stringify(fileKeys)).then(function(response){
                $scope.searchSelect("");
                $scope.startAndRefresh();
            });
        }
    };

    $scope.toggleHamburger = function(){
        if($scope.showHamburger == "showHamburger"){
            $scope.showHamburger = "";
        }
        else {
            $scope.showHamburger = "showHamburger";
        }
        window.setTimeout(function(){
            setHeaderMargin();
        },1);

    };

    $scope.searchErrors = function(e){
        if(typeof e != 'undefined'){
            e = $scope.replaceHtmlEntities(e);
            $(".foundSearch").removeClass("foundSearch");
            for(i=0;i<Object.keys($scope.data).length;++i){
                var indices = $scope.parseSearchString(e,$scope.data[i].title,false);
                if(indices[0] != null && indices.length !=0 ){
                    var Errortext = $scope.data[i].title;
                    var spanLength = 26;
                    for(j=0;j<indices.length;++j){
                        if(indices[j] != null){
                            Errortext = Errortext.slice(0, indices[j][0]+spanLength*j)
                                +"<span class='blue'>"
                                + Errortext.slice(indices[j][0]+spanLength*j,indices[j][0]+spanLength*j + indices[j][1])
                                +"</span>"
                                + Errortext.slice(indices[j][0]+spanLength*j + indices[j][1]);
                        }
                    }
                    $(".errorElement[data-key='"+$scope.data[i].key+"'] .metaData span").html(Errortext);
                    $(".errorElement[data-key='"+$scope.data[i].key+"']").removeClass("notVisible").addClass("foundSearch");

                }
                else {
                    $(".errorElement[data-key='"+$scope.data[i].key+"'] .metaData span").html($scope.data[i].title);
                    if( e.length>2){
                        $(".errorElement[data-key='"+$scope.data[i].key+"']").addClass("notVisible");
                    }
                    else {
                        $(".errorElement[data-key='"+$scope.data[i].key+"']").removeClass("notVisible");
                    }
                }
            }
            doZebra();
        }

        for(var option in $scope.options){
            var element = $("#searchList .ng-scope[data-key='"+$scope.options[option]+"']");
            if($scope.searchText && $scope.searchText.length >1 && $scope.options[option].indexOf($scope.searchText) != -1){
                element.removeClass("myHidden");
            } else {
                element.addClass("myHidden");
            }
        }
        if($("#searchList .myHidden").length <$("#searchList .ng-scope").length){
            $("#searchList").addClass("openSelect");
            window.setTimeout(function(){
                $("#searchList").removeClass("openSelect");
            },6000);
        }
        else {
            $("#searchList").removeClass("openSelect");
        }

        setTimeout(function () {
            $scope.$apply(function () {
                $scope.setErrorCount();
            });
        }, 1);

    };

    $scope.replaceHtmlEntities = function(text){
        for(var i in config){
            text = text.split(i).join(config[i]);
        }
        return text;
    };

    $scope.setErrorCount = function(){
        $scope.errorCount = $(".errorElement").size();
        $scope.criticalErrorCount = $(".errorElement.fatal").size();
        $scope.errorFilterCount = $(".foundSearch").size();
        $("title").text("PHP_ERRORS "+ $("body").attr("data-servername") +" | "+$scope.errorCount+" Fehler");
    };

    $scope.parseSearchString = function(searchStr, str, caseSensitive){
        var orStrings = searchStr.split(" || ");
        var andIndices = [];
        var orIndices = [];
        var indice = [];

        for(var i=0;i<orStrings.length;++i){
            if(orStrings[i].indexOf(" && ") != -1){
                var andString = orStrings[i].split(" && ");
                andIndices =[];
                for(var j=0;j<andString.length;++j){
                    indice = $scope.getIndicesOf(andString[j], str, caseSensitive);
                    if(indice !=null && indice.length != 0){
                        andIndices.push(indice);
                    }
                    else {
                        j=andString.length;
                        andIndices = [];
                    }
                }
            }
            else {
                indice = $scope.getIndicesOf(orStrings[i], str, caseSensitive);
                if(indice !=null){
                    orIndices.push(indice);
                }
            }

        }
        var indices = andIndices.concat(orIndices);
        var result =[];
        var k = 0;
        for (i=0;i<indices.length;++i){
            for (j=0;j<indices[i].length;++j) {
                result[k] = [];
                result[k][0] = indices[i][j][0];
                result[k][1] = indices[i][j][1];
                ++k;
            }
        }
        result.sort(function(a,b){
            return a[0]-b[0];
        });

        return result;
    };

    $scope.getIndicesOf = function(searchStr, str, caseSensitive) {
        var startIndex = 0, searchStrLen = searchStr.length;

        var indices = [];
        if (!caseSensitive) {
            str = str.toLowerCase();
            searchStr = searchStr.toLowerCase();
        }
        while (searchStr.trim() != "" && str.indexOf(searchStr, startIndex) > -1) {
            var index  = str.indexOf(searchStr, startIndex);
            indices.push([index,searchStrLen]);
            startIndex = index + searchStrLen;
        }
        if (searchStrLen>2){
            return indices;
        }
        else return null;

    };

    //TODO: Sehr redundant, kann noch optimiert werden
    $scope.parseDetailInformation = function(errorText){
        var detailHtml;
        var stackTrace = [];
        var serverData = [];
        var getData = [];
        var postData = [];
        var filesData = [];
        var cookiesData = [];
        var dateTime = 'undefined';
        var type = 'undefined';
        var category = 'undefined';
        var errorNum = 'undefined';
        var message = 'undefined';
        var script = 'undefined';
        var line = 'undefined';
        var requestURI = 'undefined';
        var httpReferer = 'undefined';
        var payLoad = '';
        var arrays = {};

        if (typeof errorText == "string") {
            var lines = errorText.split("\n");
            var j;

            for(var i=0;i<lines.length;++i){

                if(lines[i].toLowerCase().indexOf("request_uri=")!=-1){
                    requestURI = lines[i].substring(lines[i].indexOf(':')+1);
                }
                if(lines[i].toLowerCase().indexOf("http_referer=")!=-1){
                    httpReferer = lines[i].substring(lines[i].indexOf(':')+1);
                }
                if(lines[i].toLowerCase().indexOf("datetime:")==0){
                    dateTime = lines[i].substring(lines[i].indexOf(':')+1);
                }
                if(lines[i].toLowerCase().indexOf("type:")==0 && type=='undefined'){
                    type = lines[i].substring(lines[i].indexOf(':')+1);
                }
                if(lines[i].toLowerCase().indexOf("category:")==0 && category=='undefined'){
                    category = lines[i].substring(lines[i].indexOf(':')+1);
                }
                if(lines[i].toLowerCase().indexOf("script:")==0){
                    script = lines[i].substring(lines[i].indexOf(':')+1);
                }
                if(lines[i].toLowerCase().indexOf("line:")==0){
                    line = lines[i].substring(lines[i].indexOf(':')+1);
                }
                if(lines[i].toLowerCase().indexOf("errornum:")==0){
                    errorNum = lines[i].substring(lines[i].indexOf(':')+1);
                }

                if((lines[i].toLowerCase().indexOf("msg:")==0 || (lines[i].toLowerCase().indexOf("message:")==0) && message=='undefined')){
                    message = lines[i].substring(lines[i].indexOf(':')+1);
                    for(j=i+1;lines[j].toLowerCase().indexOf("script:")!=0 && lines[j].toLowerCase().indexOf("stack trace:")!=0 ;++j){
                        message += " " + lines[j];
                    }
                }

                if(lines[i][0]=="#"){
                    stackTrace.push(lines[i]);
                }

                if(lines[i].toLowerCase().indexOf("server => array") != -1){
                    for(j = i+1;lines[j][0] != ")";++j){
                        if(lines[j] !="("){
                            if(lines[j].toLowerCase().trim()=='array ('){
                                serverData.pop();
                                serverData.push(lines[j-1]+lines[j].trim());
                            }
                            else {
                                serverData.push(lines[j]);
                            }

                        }
                    }
                }
                if(lines[i].toLowerCase().indexOf("get => array") != -1){
                    for(j = i+1;lines[j][0] != ")";++j){
                        if(lines[j] !="(")getData.push(lines[j]);
                    }
                }
                if(lines[i].toLowerCase().indexOf("post => array") != -1){
                    for(j = i+1;lines[j][0] != ")";++j){
                        if(lines[j] !="(")postData.push(lines[j]);
                    }
                }
                if(lines[i].toLowerCase().indexOf("files => array") != -1){
                    for(j = i+1;lines[j][0] != ")";++j){
                        if(lines[j] !="(")filesData.push(lines[j]);
                    }
                }
                if(lines[i].toLowerCase().indexOf("cookies => array") != -1){
                    for(j = i+1;lines[j][0] != ")";++j){
                        if(lines[j] !="(")cookiesData.push(lines[j]);
                    }
                }
                if(lines[i].toLowerCase()=='array'){
                    arrays[lines[i-1]]=[];
                    for(j = i+1;lines[j][0] != ")";++j){
                        if(lines[j]!=""){
                            arrays[lines[i-1]].push(lines[j]);
                        }
                    }
                    arrays[lines[i-1]].push(")");
                }
            }
        } else if (typeof errorText == "object") {
            if (errorText.DATETIME != undefined) {
                dateTime = errorText.DATETIME;
            }
            if (errorText.level != undefined) {
                errorNum = errorText.level;
            }
            if (errorText.LINE != undefined) {
                line = errorText.LINE.toString();
            }
            if (errorText.project != undefined) {
                category = errorText.project;
            }
            if (errorText.SERVER.REQUEST_URI != undefined) {
                requestURI = errorText.SERVER.REQUEST_URI;
            }
            if (errorText.PAYLOAD != undefined) {
                payLoad = errorText.PAYLOAD.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/\\"/g, '"');
            }

            type = errorText.TYPE;
            script = errorText.SCRIPT;
            message = errorText.MSG;

            var key = null;

            if (typeof errorText.STACKTRACE == "object") {
                for (key in errorText.STACKTRACE) {
                    if (errorText.STACKTRACE.hasOwnProperty(key)) {
                        if (errorText.STACKTRACE[key] != undefined) {
                            stackTrace.push(JSON.stringify(errorText.STACKTRACE[key], null, 4));
                        }
                    }
                }
            } else {
                stackTrace = errorText.STACKTRACE.split("\n");
            }

            if (typeof errorText.SERVER == "object") {
                for (key in errorText.SERVER) {
                    if (errorText.SERVER.hasOwnProperty(key)) {
                        serverData.push(key+' => '+errorText.SERVER[key]);
                    }
                }
            }

            if (typeof errorText.GET == "object") {
                for (key in errorText.GET) {
                    if (errorText.GET.hasOwnProperty(key)) {
                        getData.push(key+' => '+errorText.GET[key]);
                    }
                }
            }

            if (typeof errorText.POST == "object") {
                for (key in errorText.POST) {
                    if (errorText.POST.hasOwnProperty(key)) {
                        postData.push(key+' => '+errorText.POST[key]);
                    }
                }
            }

            if (typeof errorText.FILES == "object") {
                for (key in errorText.FILES) {
                    if (errorText.FILES.hasOwnProperty(key)) {
                        filesData.push(key+' => '+errorText.FILES[key]);
                    }
                }
            }
        }

        detailHtml =
            '<div class="panel panel-default">'+
            '<div class="panel-heading noPointer">Fehlerinformationen</div>' +
            '<div id="ErrorInfo" class="panel-body noPointer">' +
            '<div class="infoLine"><div class="infoName">DATETIME: </div><div class="infoData">' + dateTime.trim() + '</div></div>' +
            '<div class="infoLine"><div class="infoName">TYPE: </div><div class="infoData">' + type.trim() + '</div></div>' +
            '<div class="infoLine"><div class="infoName">SCRIPT: </div><div class="infoData">' + script.trim() + '</div></div>' +
            '<div class="infoLine"><div class="infoName">LINE: </div><div class="infoData">' + line.trim() + '</div></div>' +
            '<div class="infoLine"><div class="infoName">ERRORNUM: </div><div class="infoData">' + errorNum.trim() + '</div></div>' +
            '<div class="infoLine"><div class="infoName">CATEGORY: </div><div class="infoData">' + category.trim() + '</div></div>' +
            '<div class="infoLine"><div class="infoName">REQUEST_URI: </div><div class="infoData">' + requestURI.trim() + '</div></div>' +
            '<div class="infoLine"><div class="infoName">MSG: </div><div class="infoData">' + message.trim() + '</div></div>'+
            '</div>'+
            '</div>';

        if(stackTrace.length){
            detailHtml += '<div class="panel panel-default">';
            detailHtml += '<div class="panel-heading glyphicon-plus">Stacktrace</div>';
            detailHtml += '<div class="panel-body">';
            for(i=0;i<stackTrace.length;++i){
                detailHtml += '<div class="infoLine"><div class="infoName">&nbsp;#' + i + '</div><div class="infoData">' + stackTrace[i] + '</div></div>';
            }
            detailHtml += '</div>';
            detailHtml += '</div>';
        }
        if(serverData.length){
            detailHtml += '<div class="panel panel-default">';
            detailHtml += '<div class="panel-heading glyphicon-plus">Servervariablen</div>';
            detailHtml += '<div class="panel-body">';
            for(i=0;i<serverData.length;++i){
                detailHtml += '<div class="infoLine"><div class="infoName">' + serverData[i].substring(0,serverData[i].indexOf(' => ')) + '</div><div class="infoData">' + serverData[i].substring(serverData[i].indexOf(' => ')+3) + '</div></div>';
            }
            detailHtml += '</div>';
            detailHtml += '</div>';
        }
        if(getData.length){
            detailHtml += '<div class="panel panel-default">';
            detailHtml += '<div class="panel-heading glyphicon-plus">GET</div>';
            detailHtml += '<div class="panel-body">';
            for(i=0;i<getData.length;++i){
                detailHtml += '<div class="infoLine"><div class="infoName">' + getData[i].substring(0,getData[i].indexOf(' => ')) + '</div><div class="infoData">' + getData[i].substring(getData[i].indexOf(' => ')+3) + '</div></div>';
            }
            detailHtml += '</div>';
            detailHtml += '</div>';
        }
        if(postData.length){
            detailHtml += '<div class="panel panel-default">';
            detailHtml += '<div class="panel-heading glyphicon-plus">POST</div>';
            detailHtml += '<div class="panel-body">';
            for(i=0;i<postData.length;++i){
                detailHtml += '<div class="infoLine"><div class="infoName">' + postData[i].substring(0,postData[i].indexOf(' => ')) + '</div><div class="infoData">' + postData[i].substring(postData[i].indexOf(' => ')+3) + '</div></div>';
            }
            detailHtml += '</div>';
            detailHtml += '</div>';
        }
        if(filesData.length){
            detailHtml += '<div class="panel panel-default">';
            detailHtml += '<div class="panel-heading glyphicon-plus">FILES</div>';
            detailHtml += '<div class="panel-body">';
            for(i=0;i<filesData.length;++i){
                detailHtml += '<div class="infoLine"><div class="infoName">' + filesData[i].substring(0,filesData[i].indexOf(' => ')) + '</div><div class="infoData">' + filesData[i].substring(filesData[i].indexOf(' => ')+3) + '</div></div>';
            }
            detailHtml += '</div>';
            detailHtml += '</div>';
        }
        if(cookiesData.length){
            detailHtml += '<div class="panel panel-default">';
            detailHtml += '<div class="panel-heading glyphicon-plus">COOKIES</div>';
            detailHtml += '<div class="panel-body">';
            for(i=0;i<cookiesData.length;++i){
                detailHtml += '<div class="infoLine"><div class="infoName">' + cookiesData[i].substring(0,cookiesData[i].indexOf(' => ')) + '</div><div class="infoData">' + cookiesData[i].substring(cookiesData[i].indexOf(' => ')+3) + '</div></div>';
            }
            detailHtml += '</div>';
            detailHtml += '</div>';
        }
        if (payLoad != '') {
            detailHtml += '<div class="panel panel-default">';
            detailHtml += '<div class="panel-heading glyphicon-plus">Payload</div>';
            detailHtml += '<div class="panel-body">';
            detailHtml += '<div class="infoLine">' + payLoad + '</div>';
            detailHtml += '</div>';
            detailHtml += '</div>';
        }
        $.each(arrays, function(key,value){
            if(value.length>2){
                detailHtml += '<div class="panel panel-default">';
                detailHtml += '<div class="panel-heading glyphicon-plus">'+key+'</div>';
                detailHtml += '<div class="panel-body">';
                for(i=0;i<value.length;++i){
                    detailHtml += '<div class="infoLine">' + value[i] + '</div>';
                }
                detailHtml += '</div>';
                detailHtml += '</div>';
            }

        });

        return detailHtml;
    };


    $scope.startAndRefresh();
    $scope.refreshSearchlist();
    window.setInterval(function(){
        $scope.startAndRefresh();
        $scope.refreshSearchlist();

    },$scope.refreshTime*1000);
}]);

var element = document.getElementById("headerGroup");
var headroom = new Headroom(element);
headroom.init();
$('[data-toggle="tooltip"]').tooltip();

function setHeaderMargin(){
    $("#errorList").css('margin-top',$("#headerGroup").height());
    var li = $("#tabs li");
    if(li.size()){
        if($("body").width()<721){
            li.css("width",100/li.size()+"%");
        }
        else {
            li.css("width","auto");
        }
    }
}
function doZebra(){
    var toggler = 0;
    $(".errorElement").removeClass("odd");
    $(".errorElement:not('.notVisible')").each(function(){
        if(toggler){
            $(this).addClass("odd");
        }
        toggler = ++toggler%2;
    });
}
var doIt;
window.onresize = function(){
    clearTimeout(doIt);
    doIt = setTimeout(setHeaderMargin, 100);
};
