# Benutzung

## Als Scriptsammlung

kopieren der Ordner 
**config, js, src, vendor, views**
und der Dateien
**index.php** und **style.css** und **.htaccess**
auf den Webspace

## Als Phar-Archiv (BETA)
- aufrufen der Datei "createPhar.php" in "bin" Verzeichniss
- es wird in Ordner build eine phperrors.phar Datei abgelegt
- diese auf den Webspace kopieren
- und aufrufen http://host/phperrors.phar

***Webserver umkonfigurieren das er phar Dateien als php interpretiert***

# Konfiguration

## Location
folgende Datein werden gesucht und wenn vorhanden ausgelesen

1. /etc/phperrors.config.json
2. $_SERVER['SCRIPT_NAME']/../phperrors.config.json (interessant für phar im gleicher Ordner wie die phar)
3. config/config.dist.json
4. config/config.json

##Inhalt

```json
{
  "errorPath": "/var/log/php",
  "server": {
    "server1Name" : "http://server1Url/"  
  },
  "autoloadPath" : "/../vendor/autoload.php"
}
```

**errorPath** => liest die Fehler von diesem Pfad

**server** => array mit server die als tabs oben dargestellt werden sollen

**autoloadPath** => (optional) falls das vendor dir außerhalb von phperrors liegt (z.B. wenn phperrors selbst als package dependency installiert wird)

